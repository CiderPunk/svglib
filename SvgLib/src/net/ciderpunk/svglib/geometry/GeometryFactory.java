package net.ciderpunk.svglib.geometry;

import com.badlogic.gdx.utils.ObjectMap;
import com.badlogic.gdx.utils.XmlReader;
import com.badlogic.gdx.utils.reflect.ClassReflection;
import com.badlogic.gdx.utils.reflect.Constructor;
import net.ciderpunk.svglib.SvgDoc;
import net.ciderpunk.svglib.geometry.shapes.*;

/**
 * Created by matthewlander on 01/03/2016.
 */
public class GeometryFactory {

	public static final GeometryFactory instance = new GeometryFactory();

	private GeometryFactory(){
		RegisterGeometry("g", Group.class);
		RegisterGeometry("circle", Circle.class);
		RegisterGeometry("rect", Rect.class);
		RegisterGeometry("ellipse", Ellipse.class);
		RegisterGeometry("path", Path.class);
		RegisterGeometry("text", Text.class);
	}

	private final ObjectMap<String, Class> typeMap = new ObjectMap<String, Class>();

	private void RegisterGeometry(String tagName, Class type){
		if(ClassReflection.isAssignableFrom(Geometry.class, type)){
			typeMap.put(tagName, type);
		}
	}


	public Geometry createGeometry(SvgDoc doc, Geometry parent, XmlReader.Element element) {
		Class type = typeMap.get(element.getName().toLowerCase());
		if (type != null) {
			try {
				Constructor con = ClassReflection.getConstructor(type,SvgDoc.class, Geometry.class, XmlReader.Element.class);
				return (Geometry) con.newInstance(doc, parent, element);
			} catch (Exception e) {
				//do something
			}
		}
		return null;
	}

}
