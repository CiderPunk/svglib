package net.ciderpunk.svglib.geometry;

import com.badlogic.gdx.graphics.g3d.Environment;
import com.badlogic.gdx.graphics.g3d.Model;
import com.badlogic.gdx.graphics.g3d.ModelBatch;
import com.badlogic.gdx.graphics.g3d.ModelInstance;
import com.badlogic.gdx.graphics.g3d.utils.ModelBuilder;
import com.badlogic.gdx.math.Matrix3;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.XmlReader;
import net.ciderpunk.svglib.SvgDoc;
import net.ciderpunk.svglib.geometry.shapes.Shape;
import net.ciderpunk.svglib.style.Style;
import net.ciderpunk.svglib.utils.SvgHelper;

/**
 * Created by Matthew on 09/03/2016.
 */
public class SvgRoot extends Group {


  protected final Array<Group> groups;

  public SvgRoot(SvgDoc svgDoc, Geometry parent, XmlReader.Element element) {
    super(svgDoc, parent, element);
    groups = new Array(children.size);
    for (Geometry child : children) {
      if (child instanceof Group){
        groups.add((Group) child);
      }
    }

  }

  public Group getGroup(String name){
    for (Group group : groups) {
      if (name.equals(group.id)){
        return group;
      }
    }
    return null;
  }

  public Group getGroup(int id){
    return groups.get(id);
  }

  public Array<Group> getGroups() {
    return groups;
  }

  @Override
  protected void calcCombinedMatrix(Matrix3 result){
    result.mulLeft(SvgHelper.invertMatrix);

  }

}
