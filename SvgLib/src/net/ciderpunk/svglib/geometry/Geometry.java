package net.ciderpunk.svglib.geometry;

import com.badlogic.gdx.graphics.g3d.Environment;
import com.badlogic.gdx.graphics.g3d.Model;
import com.badlogic.gdx.graphics.g3d.ModelBatch;
import com.badlogic.gdx.graphics.g3d.ModelInstance;
import com.badlogic.gdx.graphics.g3d.model.Node;
import com.badlogic.gdx.graphics.g3d.utils.ModelBuilder;
import com.badlogic.gdx.math.Matrix3;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.utils.ObjectMap;
import com.badlogic.gdx.utils.XmlReader;
import net.ciderpunk.svglib.SvgDoc;
import net.ciderpunk.svglib.SvgParseException;
import net.ciderpunk.svglib.style.Style;
import net.ciderpunk.svglib.utils.Bounds;
import net.ciderpunk.svglib.utils.SvgHelper;

/**
 * Created by matthewlander on 01/03/2016.
 */
public abstract class Geometry {
	protected Body body;
  private final Style defaultStyle = new Style(this, null);

	static int idCounter = 0;
	public final String id;
	public final String desc;
	public final ObjectMap<String, String> attributes;
	public final Geometry parent;
	protected final Style style;
	public final SvgDoc doc;
  public final Matrix3 transform = new Matrix3();
  public final Matrix3 combinedTransform = new Matrix3();
	protected Node modelNode;


	public abstract void addToBody(Body body, FixtureDef fixtureDef);


	public Geometry(SvgDoc doc, Geometry parent, XmlReader.Element element){
		//store document
		this.doc = doc;
		//store parent group
		this.parent = parent;
		//store attributes
		attributes  = element.getAttributes();
		//get description
		XmlReader.Element desc = element.getChildByName("desc");
		this.desc = desc != null ? desc.getText() : null;
		//get id..
		this.id = attributes.get("id", "element" + (idCounter++));

		//read styles
		String styleDef = attributes.get("style", null);
		style = styleDef != null ? new Style(this, styleDef) : parent != null ? parent.getStyle() : defaultStyle;
		//transform.set(invertMatrix);
		//read transformation attribute
		String transformDef = attributes.get("transform", null);
		if (transformDef != null) {
			try {
				transform.set(SvgHelper.parseTransform(transformDef, false));
			}
			catch(SvgParseException e){
			}
		}
    calcCombinedMatrix(combinedTransform);
	}



  protected void calcCombinedMatrix(Matrix3 result){
    result.mulLeft(this.transform);
    if (parent != null){
      parent.calcCombinedMatrix(result);
    }
  }


	public Style getStyle(){
		return this.style!= null ? this.style : parent!= null ? parent.getStyle() : null;
	}
	public SvgDoc getDoc() {
		return doc;
	}

	public abstract void draw(ModelBatch batch, Environment env);

	public abstract ModelInstance getModelInstance();

	public abstract Model getModel();

	public abstract void addToWorld(World world, BodyDef bodyDef, FixtureDef fixtureDef, boolean combine);

	public abstract void update();

	public abstract void createModel(ModelBuilder modelBuilder, ModelDef def);

	public abstract void extBounds(Bounds bounds);

	public Body getBody() {
		return body;
	}

}
