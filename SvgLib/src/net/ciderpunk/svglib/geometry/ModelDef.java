package net.ciderpunk.svglib.geometry;

/**
 * Created by Matthew on 31/01/2016.
 */

/**
 * settings for model generation
 */
public class ModelDef {
  public float depth = 0;
  public float verticiesPerRadiusUnit = 0.4f;
  public float smoothThreshold = 20f;
  public float zIndex =0f;

  public int minRadialVerticies = 6;
  public int maxRadialVerticies = 20;
  public ModelDef() { }
  public ModelDef(float depth, float zIndex) {
		this.depth = depth;
		this.zIndex = zIndex;
	}

}
