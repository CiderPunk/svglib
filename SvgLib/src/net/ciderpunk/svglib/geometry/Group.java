package net.ciderpunk.svglib.geometry;

import com.badlogic.gdx.graphics.g3d.Environment;
import com.badlogic.gdx.graphics.g3d.Model;
import com.badlogic.gdx.graphics.g3d.ModelBatch;
import com.badlogic.gdx.graphics.g3d.ModelInstance;
import com.badlogic.gdx.graphics.g3d.utils.ModelBuilder;
import com.badlogic.gdx.math.Matrix3;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.physics.box2d.*;
import com.badlogic.gdx.physics.box2d.joints.WeldJointDef;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.Disposable;
import com.badlogic.gdx.utils.XmlReader.*;
import net.ciderpunk.svglib.SvgDoc;
import net.ciderpunk.svglib.geometry.shapes.Shape;
import net.ciderpunk.svglib.utils.Bounds;

/**
 * Created by Matthew on 23/01/2016.
 */
public class Group extends Geometry implements Disposable {

  protected final Array<Geometry> children;
  protected Model model;

	private static final Bounds bounds = new Bounds();



	public Group(SvgDoc svgDoc, Geometry parent, Element element) {
    super(svgDoc, parent, element);
    final int count = element.getChildCount();
    children =  new Array<Geometry>(count);
    for (int i = 0; i < count; i++) {
      Geometry geo = GeometryFactory.instance.createGeometry(this.doc, this, element.getChild(i));
      if (geo != null) {
        children.add(geo);
      }
    }
  }

	@Override
  public void draw(ModelBatch batch, Environment env) {
    for (Geometry geo : children) {
      geo.draw(batch, env);
    }
  }

  @Override
  public ModelInstance getModelInstance() {
    return null;
  }

  /**
   * get shape by index
   * @param index
   * @return shape
   */
  public Geometry getChild(int index){
    return children.get(index);
  }

  /**
   * get shape by id
   * @param id
   * @return shape or null if not found
   */
  public Geometry getChild(String id){
    for (Geometry child : children) {
      if (child.id.equals(id)){
        return child;
      }
    }
    return null;
  }

  public int getChildCount(){
    return children.size;
  }

  public Array<Geometry> getChildren(){
    return children;
  }

	public Array<Shape> getShapesRecursive(Array<Shape> arr){
		for (Geometry child : children) {
			if (child instanceof Shape){
				arr.add((Shape)child);
			}
			else if(child instanceof Group){
				((Group)child).getShapesRecursive(arr);
			}
		}
		return arr;
	}


  public void buildModel(ModelDef def){
    //build 3D models of environment
    ModelBuilder modelBuilder = new ModelBuilder();
    modelBuilder.begin();
		this.createModel(modelBuilder, def);
    model = modelBuilder.end();
  }

  @Override
  public void dispose() {
    if (model != null){
      model.dispose();
    }
  }



	/**
	 * proly of limited use, returns array of ll child model instances
	 * @return
	 */
	/*
  public Array<ModelInstance> getModelInstances() {
		Array<ModelInstance> models = new Array<ModelInstance>(children.size);
		for (Geometry child : children) {
			ModelInstance modelInstance = child.getModelInstance();
			if (model != null) {
				models.add(modelInstance);
			}
		}
    return models;
  }
*/

  public void update(){
    for (Geometry child : children) {
      child.update();
    }
  }

	@Override
	public void createModel(ModelBuilder modelBuilder, ModelDef def) {
		modelNode = modelBuilder.node();
		modelNode.id = this.id;
		for (Geometry child : children) {
			child.createModel(modelBuilder, def);
		}
	}



	public Model getModel() {
    return this.model != null ? this.model : this.parent != null ? parent.getModel() : null;
  }


	@Override
	public void extBounds(Bounds bounds) {
		for (Geometry child : children) {
			child.extBounds(bounds);
		}
	}


	public void weld(){
		if (body != null){
			WeldJointDef weldDef = new WeldJointDef();

			for (Geometry child : children) {
				weldDef.initialize(body, child.getBody(), Vector2.Zero);
			}
		}
	}

/*
	public void addToWorld(World world, BodyDef bodyDef, FixtureDef fixtureDef, boolean root) {
		if (!root){
			bounds.reset();
			extBounds(bounds);
			bounds.getCenter(bodyDef.position);
			bodyDef.angle = 0;
			body = world.createBody(bodyDef);
			this.addToBody(body, fixtureDef);
		}
		for (Geometry child : children) {
			child.addToWorld(world, bodyDef, fixtureDef);
		}
	}
	*/

	public void addToWorld(World world, BodyDef bodyDef, FixtureDef fixtureDef, boolean combine) {
		if (combine){
			bounds.reset();
			extBounds(bounds);
			bounds.getCenter(bodyDef.position);
			bodyDef.angle = 0;
			body = world.createBody(bodyDef);
			this.addToBody(body, fixtureDef);
		}
		else{
			for (Geometry child : children) {
				child.addToWorld(world, bodyDef, fixtureDef, combine);
			}
		}
	}


	/*
	@Override
	public void addToBody(Body body, FixtureDef fixtureDef) {
		CircleShape shape = new CircleShape();
		shape.setRadius(2);
		fixtureDef.shape = shape;
		body.createFixture(fixtureDef);
		shape.dispose();
	}
	*/
	@Override
	public void addToBody(Body body, FixtureDef fixtureDef) {

		for (Geometry child : children) {
			child.addToBody(body, fixtureDef);
		}
	}

  public SvgDoc getDoc() {
    return doc;
  }

}
