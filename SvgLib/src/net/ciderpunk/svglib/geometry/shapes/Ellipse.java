package net.ciderpunk.svglib.geometry.shapes;

import com.badlogic.gdx.graphics.g3d.Model;
import com.badlogic.gdx.graphics.g3d.utils.ModelBuilder;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.utils.XmlReader;
import net.ciderpunk.svglib.SvgDoc;
import net.ciderpunk.svglib.geometry.Geometry;
import net.ciderpunk.svglib.geometry.Group;
import net.ciderpunk.svglib.geometry.ModelDef;
import net.ciderpunk.svglib.utils.Bounds;

/**
 * Created by Matthew on 23/01/2016.
 */
public class Ellipse extends Shape {

  final Vector2 radius = new Vector2();


  @Override
  public void addToBody(Body body, FixtureDef fixtureDef) {

  }

  public Ellipse(SvgDoc doc, Geometry parent, XmlReader.Element element) {
    super(doc, parent, element);


    position.x = Float.parseFloat(element.getAttribute("cx", "0"));
    position.y = Float.parseFloat(element.getAttribute("cy", "0"));
    radius.x = Float.parseFloat(element.getAttribute("rx", "0"));
    radius.y = Float.parseFloat(element.getAttribute("ry", "0"));

  }

  /*
  @Override
  public void drawShape(ShapeRenderer shaper) {
    shaper.ellipse(position.x - radius.x, position.y - radius.y, radius.x * 2, radius.y * 2);
  }
*/
  @Override
  public void createModel(ModelBuilder modelBuilder, ModelDef modelDef) {
    super.createModel(modelBuilder, modelDef);
  }

  @Override
  public void extBounds(Bounds bounds) {

  }


}
