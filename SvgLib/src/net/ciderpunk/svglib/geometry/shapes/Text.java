package net.ciderpunk.svglib.geometry.shapes;

import com.badlogic.gdx.graphics.g3d.Environment;
import com.badlogic.gdx.graphics.g3d.Model;
import com.badlogic.gdx.graphics.g3d.ModelBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.utils.XmlReader;
import net.ciderpunk.svglib.SvgDoc;
import net.ciderpunk.svglib.geometry.Geometry;
import net.ciderpunk.svglib.geometry.Group;
import net.ciderpunk.svglib.utils.Bounds;

/**
 * Created by matthewlander on 27/02/2016.
 */
public class Text extends Shape
{
	public final float rotation;
	public final String text;

	@Override
	public void addToBody(Body body, FixtureDef fixtureDef) {	this.body = body;}

	public Text(SvgDoc doc, Geometry parent, XmlReader.Element element) {
		super(doc, parent, element);
		XmlReader.Element tspan = element.getChildByName("tspan");
		text = tspan != null ? tspan.getText() : "";
		position.x = Float.parseFloat(element.getAttribute("x"));
		position.y = Float.parseFloat(element.getAttribute("y"));
		transformedPosition.set(position).mul(combinedTransform);
		rotCorrection = transform.getRotationRad();
		//rotation = transform.getRotation();
		rotation = rotCorrection;
	}

	/**
	 * not drawing texts
	 * @param batch
	 * @param env
	 */
	@Override
	public void draw(ModelBatch batch, Environment env) {
	}

	@Override
	public Model getModel() {
		return null;
	}

	@Override
	public void extBounds(Bounds bounds) {

	}
}
