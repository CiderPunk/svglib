package net.ciderpunk.svglib.geometry.shapes;

import com.badlogic.gdx.graphics.g3d.Environment;
import com.badlogic.gdx.graphics.g3d.Model;
import com.badlogic.gdx.graphics.g3d.ModelBatch;
import com.badlogic.gdx.graphics.g3d.ModelInstance;
import com.badlogic.gdx.graphics.g3d.model.Node;
import com.badlogic.gdx.graphics.g3d.utils.ModelBuilder;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.Matrix3;
import com.badlogic.gdx.math.Matrix4;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.physics.box2d.*;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.ObjectMap;
import com.badlogic.gdx.utils.XmlReader.*;
import com.badlogic.gdx.utils.reflect.ClassReflection;
import com.badlogic.gdx.utils.reflect.Constructor;
import net.ciderpunk.svglib.SvgDoc;
import net.ciderpunk.svglib.SvgParseException;
import net.ciderpunk.svglib.geometry.Geometry;
import net.ciderpunk.svglib.geometry.Group;
import net.ciderpunk.svglib.geometry.ModelDef;
import net.ciderpunk.svglib.geometry.shapes.path.PathPart;
import net.ciderpunk.svglib.style.Style;
import net.ciderpunk.svglib.utils.SvgHelper;

/**
 * Created by Matthew on 23/01/2016.
 */
public abstract class Shape extends Geometry{
  protected float rotCorrection = 0f;
  public final Vector2 position = new Vector2();
  public final Vector2 transformedPosition = new Vector2();
  protected Fixture fixture;

  public float zIndex;

  ModelInstance modelInstance;

  public Shape(SvgDoc doc, Geometry parent, Element element) {
    super(doc, parent, element);
  }


  protected Vector2 transformV2(Vector2 v) {
    return transformV2(v.x, v.y);
  }

  private static final Vector2 temp = new Vector2();
  protected Vector2 transformV2(float x, float y) {
    return temp.set(x,y).mul(transform);
  }

  private final static Matrix4 tempM4 = new Matrix4();


  @Override
  public void addToWorld(World world, BodyDef bodyDef, FixtureDef fixtureDef, boolean combine){
    bodyDef.position.set(transformedPosition);
    bodyDef.angle = 0;
    body = world.createBody(bodyDef);
    this.addToBody(body, fixtureDef);
  }

  public void createModel(ModelBuilder modelBuilder, ModelDef modelDef){
    zIndex = modelDef.zIndex;
  }

  public ModelInstance getModelInstance(){
    if (modelInstance == null && modelNode != null){
      modelInstance = new ModelInstance(parent.getModel(), this.id);
      //modelInstance.transform.set(this.drawTransform);
      modelInstance.transform.translate(this.transformedPosition.x, this.transformedPosition.y, zIndex);
    }
    return modelInstance;
  }

  public static int createVertex(float[] vertexBuffer, int i, Vector2 position, Vector3 normal, Vector2 uv){
    return createVertex(vertexBuffer, i, position.x, position.y, 0f, normal.x,normal.y,normal.z,uv.x,uv.y);
  }

  public static int createVertex(float[] vertexBuffer, int i, float x, float y, float z, Vector3 normal, Vector2 uv){
    return createVertex(vertexBuffer, i, x,y,z,normal.x,normal.y,normal.z,uv.x,uv.y);
  }

  public static int createVertex(float[] vertexBuffer, int i, float x, float y, float z, float nx, float ny, float nz, Vector2 uv){
    return createVertex(vertexBuffer, i, x,y,z,nx,ny,nz,uv.x,uv.y);
  }

  protected static int createVertex(float[] vertexBuffer, int i, float x, float y, float z, float nx, float ny, float nz, float u, float v){
    vertexBuffer[i++]  = x;
    vertexBuffer[i++]  = y;
    vertexBuffer[i++]  = z;
    //normal
    vertexBuffer[i++] = nx;
    vertexBuffer[i++] = ny;
    vertexBuffer[i++] = nz;
    //UV
    vertexBuffer[i++] = u;
    vertexBuffer[i++] = v;
    return i;
  }

  public void draw(ModelBatch batch, Environment env) {
    ModelInstance mdl = getModelInstance();
    if (mdl!= null) {
      batch.render(mdl, env);
    }
  }

  public SvgDoc getDoc() {
    return parent.getDoc();
  }

  public void update() {
    Vector2 pos = body.getPosition();
    getModelInstance().transform.setToRotationRad(Vector3.Z, body.getAngle() - rotCorrection).setTranslation(pos.x,pos.y, zIndex);
  }


  @Override
  public Model getModel() {
    return parent.getModel();
  }
}
