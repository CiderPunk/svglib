package net.ciderpunk.svglib.geometry.shapes;

import com.badlogic.gdx.graphics.g3d.utils.ModelBuilder;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.Matrix3;
import com.badlogic.gdx.math.Matrix4;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.*;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.XmlReader;
import net.ciderpunk.svglib.SvgDoc;
import net.ciderpunk.svglib.geometry.Geometry;
import net.ciderpunk.svglib.geometry.Group;
import net.ciderpunk.svglib.geometry.ModelDef;
import net.ciderpunk.svglib.geometry.shapes.path.PathPart;
import net.ciderpunk.svglib.utils.Bounds;

/**
 * Created by Matthew on 23/01/2016.
 */
public class Rect extends Shape {

  private final Vector2 scale = new Vector2();
  private final Vector2 size = new Vector2();
  private final PathPart pathPart;

	public Rect(SvgDoc doc, Geometry parent, XmlReader.Element element) {
    super(doc, parent, element);
    rotCorrection = combinedTransform.getRotationRad();
    size.x = Float.parseFloat(element.getAttribute("width"));
    size.y = Float.parseFloat(element.getAttribute("height"));
    position.x = Float.parseFloat(element.getAttribute("x")) + (size.x / 2);
    position.y = Float.parseFloat(element.getAttribute("y")) + (size.y / 2);
    transformedPosition.set(position).mul(combinedTransform);

    float hW = size.x / 2f;
    float hH = size.y / 2f;
    //create array of rect points
    Array<Vector2> points = new Array(new Vector2[]{new Vector2(-hW, hH), new Vector2(hW, hH), new Vector2(hW, -hH),new Vector2(-hW, -hH)});
    //create path part
    pathPart = new PathPart(this,this.id, points, true);
  }

	@Override
	public void addToBody(Body body, FixtureDef fixtureDef) {
		this.body = body;
		pathPart.addToBody(body, fixtureDef);
	}

  @Override
  public void addToWorld(World world, BodyDef bodyDef, FixtureDef fixtureDef, boolean combine) {
		body = world.createBody(bodyDef);
		bodyDef.position.set(transformedPosition);
		this.combinedTransform.getScale(scale);
		if (scale.x == scale.y || rotCorrection == 0) {
			PolygonShape shape = new PolygonShape();
			bodyDef.angle = rotCorrection;
			shape.setAsBox(size.x / 2, size.y / 2);
			fixtureDef.shape = shape;
			body = world.createBody(bodyDef);
			body.createFixture(fixtureDef);
			shape.dispose();
		}
		else{
			addToBody(body,fixtureDef);
		}
  }

  @Override
  public void createModel(ModelBuilder modelBuilder, ModelDef modelDef){
		super.createModel(modelBuilder, modelDef);
    modelNode = modelBuilder.node();
    modelNode.id = this.id;
    //build model using path part's functions..
    pathPart.buildFace(modelBuilder, modelDef);
    if (modelDef.depth != 0) {
      pathPart.buildSkirt(modelBuilder, modelDef);
    }
  }

	@Override
	public void extBounds(Bounds bounds) {
		pathPart.extBounds(bounds, Vector2.Zero);
	}

}
