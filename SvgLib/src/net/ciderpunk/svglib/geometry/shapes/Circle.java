package net.ciderpunk.svglib.geometry.shapes;

import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.VertexAttributes;
import com.badlogic.gdx.graphics.g3d.Material;
import com.badlogic.gdx.graphics.g3d.utils.MeshPartBuilder;
import com.badlogic.gdx.graphics.g3d.utils.ModelBuilder;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Matrix3;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.physics.box2d.*;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.XmlReader;
import com.badlogic.gdx.utils.XmlReader.*;
import net.ciderpunk.svglib.SvgDoc;
import net.ciderpunk.svglib.geometry.Geometry;
import net.ciderpunk.svglib.geometry.Group;
import net.ciderpunk.svglib.geometry.ModelDef;
import net.ciderpunk.svglib.geometry.shapes.path.PathPart;
import net.ciderpunk.svglib.utils.Bounds;
import net.ciderpunk.svglib.utils.SvgHelper;

/**
 * Created by Matthew on 23/01/2016.
 */
public class Circle extends Shape {

  float  radius;

  @Override
  public void addToBody(Body body, FixtureDef fixtureDef) {
    this.body = body;
    CircleShape shape = new CircleShape();
    shape.setRadius(radius);
    fixtureDef.shape = shape;
    body.createFixture(fixtureDef);
    shape.dispose();
  }

  public Circle(SvgDoc doc, Geometry parent, XmlReader.Element element) {
    super(doc, parent, element);
    position.x = Float.parseFloat(element.getAttribute("cx", "0"));
    position.y = Float.parseFloat(element.getAttribute("cy", "0"));
    transformedPosition.set(position).mul(combinedTransform);
    //position.mul(transform);
    radius = Float.parseFloat(element.getAttribute("r", "0"));
  }


  final static Vector2 uvCoord = new Vector2();
//  final static Vector2 tempV2 = new Vector2();



  @Override
  public void createModel(ModelBuilder modelBuilder, ModelDef modelDef){
		super.createModel(modelBuilder, modelDef);

		int rimVertCount = MathUtils.ceil((float) this.radius  * modelDef.verticiesPerRadiusUnit);

		MathUtils.clamp(rimVertCount, modelDef.minRadialVerticies, modelDef.maxRadialVerticies);
    //create array of rect points
    Array<Vector2> points = new Array<Vector2>(rimVertCount);
    Array<Vector2> uv = new Array(rimVertCount);

    //radians to rotate around per vertex
    float radsPerVert = MathUtils.PI2 / rimVertCount;

    Matrix3 uvTransform = this.style.getTransform();

    //create circumference
    for (int i = 0; i < rimVertCount; i++) {
      Vector2 point = new Vector2(0,radius).rotateRad(i * radsPerVert);
      uv.add(new Vector2(point).add(this.position).mul(uvTransform));
      points.add(point);
    }

    if (SvgHelper.getDirection(points)){
      points.reverse();
      uv.reverse();
    }
    //create path part
    PathPart part = new PathPart(this,this.id, points, true);

    modelNode = modelBuilder.node();
    modelNode.id = this.id;

    //this.createFace(points,uv, modelBuilder, modelDef);
    part.buildFace(modelBuilder, modelDef);
    if (modelDef.depth != 0) {
      part.buildSkirt(modelBuilder, modelDef);
    }
  }

  @Override
  public void extBounds(Bounds bounds) {
    bounds.ext(this.transformedPosition.x + radius, this.transformedPosition.y + radius,0);
    bounds.ext(this.transformedPosition.x - radius, this.transformedPosition.y - radius,0);
  }

}
