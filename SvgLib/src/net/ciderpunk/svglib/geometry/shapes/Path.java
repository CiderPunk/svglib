package net.ciderpunk.svglib.geometry.shapes;

import com.badlogic.gdx.graphics.g3d.utils.ModelBuilder;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.Matrix4;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.*;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.XmlReader;
import net.ciderpunk.svglib.SvgDoc;
import net.ciderpunk.svglib.SvgParseException;
import net.ciderpunk.svglib.geometry.Geometry;
import net.ciderpunk.svglib.geometry.Group;
import net.ciderpunk.svglib.geometry.ModelDef;
import net.ciderpunk.svglib.geometry.shapes.path.PathPart;
import net.ciderpunk.svglib.utils.Bounds;
import net.ciderpunk.svglib.utils.SvgHelper;

/**
 * Created by Matthew on 23/01/2016.
 */
public class Path extends net.ciderpunk.svglib.geometry.shapes.Shape {

  public static final int START_VERTEX_CAPACITY = 200;

  Array<PathPart> parts;

  /**
   * get number of parts in this path
   * @return size
   */
  public int getPartCount(){return parts.size;}

  /**
   * gets part by index
   * @param i index
   * @return PathPart
   */
  public PathPart getPart(int i){
    return parts.get(i);
  }


  public Path(SvgDoc doc, Geometry parent, XmlReader.Element element) throws SvgParseException {
    super(doc, parent, element);

    //reset transform...
    //transform.idt();
    String pathDef = element.getAttribute("d", null);
    if (pathDef == null){
      throw new SvgParseException("Expected \"d\" attribute in path");
    }
    parts = parsePath(pathDef);
    //get bounds of all parts
    Bounds bounds=  new Bounds();
    for (PathPart part : parts) {
      part.extBounds(bounds, Vector2.Zero);
    }
    position.set(bounds.getCenter().x, bounds.getCenter().y);
    //get transformed position - no need to transform as the coords it's generated from are alreayd transformed! (yay)
    transformedPosition.set(position);
    //recenter all the parts
    for (PathPart part : parts) {
      part.setCenter(position);
    }
  }




  protected enum ParseState {
    MoveTo,
    LineTo,
    Horizontal,
    Vertcal,
    Curve,
  }

  static final Vector2 last = new Vector2();
 // static final Vector2 temp = new Vector2();


  protected static Vector2 parseCoord(String val) {
    String[] parts = val.split(",");
    if (parts.length != 2) {
      return null;
    }
    return new Vector2(Float.parseFloat(parts[0]), Float.parseFloat(parts[1]));
    //return temp.set(Float.parseFloat(parts[0]), Float.parseFloat(parts[1]));
  }



  public Array<PathPart> parsePath(String pathDef){
    //start from 0,0
    last.setZero();
    //new array
    Array<PathPart> parts = new Array<PathPart>();
    Array<Vector2> points = new Array<Vector2>(true, 100);

    //split into SVG path commands...
    String[] components = pathDef.split("\\s+");
    //start in moveto mode
    ParseState state = ParseState.MoveTo;
    int count=0;
    Vector2 point = Vector2.Zero;

    int skip = 0;
    boolean relative = false;

    for (String component : components) {
      switch (component.charAt(0)) {
        case 'C': //curve , skip control verticies...
          state = ParseState.Curve;
          relative = false;
          skip = 2;
          break;
        case 'c': //same
          state = ParseState.Curve;
          skip = 2;
          relative = true;
        case 'M': //move to
          state = ParseState.MoveTo;
          relative = false;
          break;
        case 'm':
          state = ParseState.MoveTo;
          relative = true;
          break;
        case 'l': //draw line...
          state = ParseState.LineTo;
          relative = true;
          break;
        case 'L':
          state = ParseState.LineTo;
          relative = false;
          break;
        case 'z':
        case 'Z': //end path part, close loop
          //make sure everything's clockwise..
          SvgHelper.makeClockwise(points);
          parts.add(new PathPart(this, this.id + "_" + (count++), points));
          point = null;
          //set to start post
          last.set(points.get(0));
          points = new Array<Vector2>();
          //points.clear();
          break;
        default:

          switch (state) {
            case MoveTo:
            case LineTo:
              point = parseCoord(component);
              if (relative && point!=null) {
                point.add(last);
              }

              break;
            case Curve:
              //ignore control verticies, want every third point
              if (--skip> 0) {
                point = null;
                break;
              }
              point = parseCoord(component);
              if (relative){
                point.add(last);
              }
              skip = 2;
              break;
            case Horizontal:
              try {
                float x = Float.parseFloat(component);
                point = new Vector2(last).add(x, 0);
              }
              catch (NumberFormatException ex) {
                point = null;
              }
              break;

            case Vertcal:
              try {
                float y = Float.parseFloat(component);
                point = new Vector2(last).add(0,y);
              }
              catch (NumberFormatException ex) {
                point = null;
              }
              break;
          }
          if (point != null) {
            last.set( point);
            //invert 'cos svg Y is down
            //point.mul(net.ciderpunk.svglib.geometry.shapes.Shape.invertMatrix);

            points.add(point);
          }

      }
    }
    return parts;
  }



  static final Matrix4 tempM4 = new Matrix4();



  @Override
  public void addToBody(Body body, FixtureDef fixtureDef) {
    this.body = body;
    for (PathPart part : parts) {
      part.addToBody(body, fixtureDef);
    }
  }


  @Override
  public void createModel(ModelBuilder modelBuilder, ModelDef modelDef){
    super.createModel(modelBuilder, modelDef);
    modelNode = modelBuilder.node();
    modelNode.id = this.id;
    //build model using path part's functions..
    for (PathPart part : parts) {
      part.buildFace(modelBuilder, modelDef);
      if (modelDef.depth != 0) {
        part.buildSkirt(modelBuilder, modelDef);
      }
    }
  }

  @Override
  public void extBounds(Bounds bounds) {
    for (PathPart part : parts) {
      part.extBounds(bounds, this.position);
    }
  }


}
