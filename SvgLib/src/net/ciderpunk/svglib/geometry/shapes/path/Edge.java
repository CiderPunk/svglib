package net.ciderpunk.svglib.geometry.shapes.path;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.FloatArray;

import java.util.Iterator;

/**
 * Created by Matthew on 28/01/2016.
 */
public class Edge {

  final Triangle owner;
  final short leftPoint, rightPoint;
  final int position;
  protected Edge link = null;

  public Edge(Triangle triangle, short i1, short i2, int position) {
    this.position =  position;
    owner = triangle;
    leftPoint = i1;
    rightPoint = i2;
  }

  /**
   * returns point to the left of the edge facing out if not linked, or the left edge of the next poly around if it is
   * @return indicies of point
   */
  protected short getLeftPoint(){
    if (link == null){
      return leftPoint;
    }
    return link.getCcwPoint();
  }
  /**
   * returns point to the right of the edge facing out if not linked, or the right most point on the out-facing edge of the next poly around if it is linked
   * @return indicies of point
   */
  protected short getRightPoint(){
    if (link == null){
      return rightPoint;
    }
    return link.getCwPoint();
  }

  /**
   * returns next point clockwise around the aggregate polygon
   * @return indicies of point
   */
  public short getCwPoint(){
    return owner.getCwIndecies(this.position);
  }

  /**
   * returns next point counterclockwise around the aggregate polygon
   * @return indicies of point
   */
  public short getCcwPoint(){
    return owner.getCcwIndecies(this.position);
  }


  public void  findEdgeJoins(Array<Triangle> unused, Array<Vector2> points) {
    if (this.link == null){

      final Iterator<Triangle> iterator = unused.iterator();

      while(iterator.hasNext()){
        Triangle triangle = iterator.next();
        Edge candidate = triangle.testJoins(this);
        if (candidate != null){
          //test cross products to check for a convex seam on clockwise and counterclockwise verticies
          Vector2 candidatePoint = points.get(candidate.getCcwPoint());
          Vector2 ccwPoint = points.get(this.getCcwPoint());
          Vector2 cwPoint = points.get(this.getCwPoint());

          if (testCrossProduct(ccwPoint, points.get(leftPoint), candidatePoint)
                  && testCrossProduct(candidatePoint,points.get(rightPoint), cwPoint)){
            //cross linkId these suckazz
            link = candidate;
            candidate.link = this;
            //remove this triangle from the unused list
            iterator.remove();
            candidate.owner.findJoins(unused, points);
            break;
          }
        }
      }
    }
  }

  private final Vector2 v1 = new Vector2();
  private final Vector2 v2 = new Vector2();

  /**
   * tests if the two vectors defined by start pivot and end have a angle > 180 (> 0) or less meaning concaveness
   * @param start
   * @param pivot
   * @param end
   * @return
   */
  private boolean testCrossProduct(Vector2 start, Vector2 pivot, Vector2 end) {
    v1.set(pivot).sub(start);
    v2.set(end).sub(pivot);
    return v1.crs(v2) < 0;
  }

  /**
   * tests if an edge joins this one
   * @param edge
   * @return true on join
   */
  public boolean testJoin(Edge edge) {
    return (edge.leftPoint == this.rightPoint && edge.rightPoint == this.leftPoint);
  }

  public void getPoint(FloatArray pointBuffer, Array<Vector2> points) {

    if (link!= null) {
      //get points for linked triangle
      link.owner.getIndicies(pointBuffer, points, link.position);
    }
    else {
      Vector2 thisPoint = points.get(this.rightPoint);
      pointBuffer.add(thisPoint.x);
      pointBuffer.add(thisPoint.y);
    }

  }
}
