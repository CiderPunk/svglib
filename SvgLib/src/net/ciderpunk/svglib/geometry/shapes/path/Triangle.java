package net.ciderpunk.svglib.geometry.shapes.path;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.FloatArray;

import java.util.Arrays;

/**
 * Created by Matthew on 28/01/2016.
 */
public class Triangle {

  final Edge[] edges = new Edge[3];

  public Triangle(short[] verts){
    Arrays.sort(verts);
    edges[0] = new Edge(this,verts[0],verts[1], 0);
    edges[1] = new Edge(this,verts[1],verts[2], 1);
    edges[2] = new Edge(this,verts[2],verts[0], 2);
  }

  public void findJoins(Array<Triangle> unnused, Array<Vector2> points){
    for (Edge edge : edges) {
      edge.findEdgeJoins(unnused, points);
    }
  }



  public Edge testJoins(Edge edge) {
    for (Edge myEdge : edges) {
      if (myEdge.testJoin(edge)){
        return myEdge;
      }
    }
    //no joins
    return null;
  }

  public void getIndicies(FloatArray pointBuffer, Array<Vector2> points) {
    for (int i =0; i < 3; i++) {
      edges[i].getPoint(pointBuffer, points);
    }
  }

  public void getIndicies(FloatArray pointBuffer, Array<Vector2> points, int startEdge) {
    for (int i = 1; i < 3; i++) {
      edges[(startEdge + i) % 3].getPoint(pointBuffer, points);
    }
  }

  public short getCwIndecies(int position) {
    return edges[(position + 1) % 3].getRightPoint();
  }

  public short getCcwIndecies(int position) {
    return edges[(position + 2) % 3].getLeftPoint();
  }


}

