package net.ciderpunk.svglib.geometry.shapes.path;

import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.VertexAttributes;
import com.badlogic.gdx.graphics.g3d.Material;
import com.badlogic.gdx.graphics.g3d.utils.MeshPartBuilder;
import com.badlogic.gdx.graphics.g3d.utils.ModelBuilder;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.*;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.Fixture;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.PolygonShape;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.FloatArray;
import com.badlogic.gdx.utils.ShortArray;
import net.ciderpunk.svglib.geometry.ModelDef;
import net.ciderpunk.svglib.geometry.shapes.Shape;
import net.ciderpunk.svglib.utils.Bounds;
import net.ciderpunk.svglib.utils.SvgHelper;

/**
 * Created by Matthew on 23/01/2016.
 */
public class PathPart {

  //public final Vector2 position = new Vector2();
  public final String id;
  Shape parent;
  Array<Vector2> points;
  Array<Vector2> uvs;
  float[] floatPoints;
  boolean floatPointsDirty = true;

  static final Bounds bounds = new Bounds();
  static final Vector2 center = new Vector2();

	public PathPart(Shape parent, String id, Array<Vector2> orig){
		this(parent,id,orig,false);
	}


  public PathPart(Shape parent, String id, Array<Vector2> orig, boolean recenter) {
    this.parent = parent;
    this.id = id;
    this.points = new Array<Vector2>(orig.size);
    uvs = new Array<Vector2>(orig.size);
    for (Vector2 point : orig) {
      //just store non-transformed position for now!
      uvs.add(new Vector2(point).add(parent.position));
      this.points.add(new Vector2(point).add(parent.position).mul(parent.combinedTransform));
    }
    if(!SvgHelper.getDirection(this.points)){
      points.reverse();
      //uvs.reverse();
    }
		//clean this up plx
		if(recenter) {
      //reset bounds
      bounds.reset();
      //extend bounds by points
			this.extBounds(bounds, Vector2.Zero);
      //get center....
			bounds.getCenter(center);
      //recenter points
			for (Vector2 point : points) {
				point.sub(center);
			}
		}
  }


  private Vector2[] normals;

  public Vector2[] getNormals(){
    if (normals == null){
      normals = new Vector2[this.points.size];
      //calc normals
      for (int i = 0; i < points.size; i++) {
        normals[i] = new Vector2(getPointCoord(i + 1)).sub(getPointCoord(i)).nor();
      }
    }
    return normals;
  }

  /**
   * returns points as array of floats
   * @return
   */
  public float[] getFloatArray(){
    if (floatPointsDirty){
      if (floatPoints == null) {
        floatPoints = new float[points.size * 2];
      }
      int i =0;
      for (Vector2 point : points) {
        floatPoints[i++] = point.x;
        floatPoints[i++] = point.y;
      }
      floatPointsDirty = false;
    }
    return floatPoints;
  }

  ShortArray triangleIndicies;
  public ShortArray getTriangles(){
    if (triangleIndicies == null) {
      EarClippingTriangulator clipper = new EarClippingTriangulator();
      triangleIndicies = clipper.computeTriangles(getFloatArray());
      triangleIndicies.reverse();
    }
    return triangleIndicies;
  }


  private Vector2 getPointNormal(int index){
    while (index < 0){index+=points.size;}
    index = index % points.size;
    return getNormals()[index];
  }

  private Vector2 getPointCoord(int index){
    while (index < 0){index+=points.size;}
    index = index % points.size;
    return points.get(index);
  }

  public void drawPath(ShapeRenderer shaper) {
    shaper.polygon(getFloatArray());
  }

  private static final Vector2 v1 = new Vector2();
  private static final Vector2 v2 = new Vector2();


  private boolean isComplex(){
    for (int i = 0; i < points.size; i++) {
      v1.set(getPointCoord(i+1)).sub(getPointCoord(i));
      v2.set(getPointCoord(i+2)).sub(getPointCoord(i+1));
      float cross = v1.crs(v2);
      if (cross < 0) {
        return true;
      }
    }
    return false;
  }

 // static final Vector2 temp = new Vector2();




  public void extBounds(Bounds bounds, Vector2 position) {
    for (Vector2 point : points) {
      bounds.ext(point,position);
    }
  }


  public void setCenter(Vector2 center) {
    for (Vector2 point : points) {
      point.sub(center);
    }
  }

  public void addToBody(Body body, FixtureDef fixtureDef) {
    //if (false){
    if (!isComplex()){
      PolygonShape shape = new PolygonShape();
      shape.set(getFloatArray());
      fixtureDef.shape = shape;
      body.createFixture(fixtureDef);
      shape.dispose();
    }
    else {
      addToBodyComplex(body, fixtureDef);
    }
  }


  /**
   * decompose concave shape into component convex shapes and add them to body fixture
   * @param body
   * @param fixtureDef
   */
  private void addToBodyComplex(Body body, FixtureDef fixtureDef) {
    ShortArray indicies = getTriangles();
    Array<Triangle> tris = new Array(indicies.size / 3);
    for (int i = 0; i < indicies.size; i+= 3) {
      tris.add(new Triangle(new short[]{indicies.get(i), indicies.get(i+1), indicies.get(i+2)}));
    }
    FloatArray pointBuffer = new FloatArray(this.points.size * 2);
    PolygonShape shape = new PolygonShape();

    while (tris.size > 0){
      Triangle active = tris.pop();
      active.findJoins(tris, this.points);
      pointBuffer.clear();

     // pointBuffer = new FloatArray(true, this.points.size * 2);
      active.getIndicies(pointBuffer, this.points);

      //polys.add(pointBuffer);

      shape.set(pointBuffer.toArray());
      fixtureDef.shape = shape;
      body.createFixture(fixtureDef);

    }
    shape.dispose();
  }


  final static Vector2 coord = new Vector2();
  final static Vector2 uv = new Vector2();
  final static Vector2 leftNormal = new Vector2();
  final static Vector2 rightNormal = new Vector2();


  public void buildSkirt(ModelBuilder modelBuilder, ModelDef modelDef){
    float[] vertexBuffer = new float[this.points.size * 4 * 8];
    short[] edgeBuffer = new short[this.points.size * 6];
    int pos = 0;
    int edgePos = 0;
    float texOffset = 0f;

    //Material mat = parent.getStyle().getMaterial();
    Matrix3 trans = parent.getStyle().getEdgeTransfom();

    //create verts
    for (int i = 0; i < points.size; i++) {
      leftNormal.set(getPointNormal(i-1));
      Vector2 norm = getPointNormal(i);
      rightNormal.set(getPointNormal(i+1));

      Vector2 left = getPointCoord(i);
      Vector2 right = getPointCoord(i + 1);

      //if angle between left normal and this is less than threshold...
      if (norm.angle(leftNormal) < modelDef.smoothThreshold){
        //set left normal to average of the two
        leftNormal.add(norm).scl(0.5f);
      }
      else{
        leftNormal.set(norm);
      }
      if (norm.angle(rightNormal) < modelDef.smoothThreshold){
        //set left normal to average of the two
        rightNormal.add(norm).scl(0.5f);
      }
      else{
        rightNormal.set(norm);
      }

      //get positive depth value
      float depth = modelDef.depth < 0 ? -modelDef.depth : modelDef.depth;

      uv.set(texOffset,0f).mul(trans);
      pos = Shape.createVertex(vertexBuffer, pos, left.x, left.y, 0f, leftNormal.x, leftNormal.y, 0f, uv);
      uv.set(texOffset,depth).mul(trans);
      pos = Shape.createVertex(vertexBuffer, pos, left.x, left.y, modelDef.depth, leftNormal.x, leftNormal.y, 0f, uv);
      //get length of edge
      texOffset+= coord.set(right).sub(left).len();
      uv.set(texOffset,0f).mul(trans);
      pos = Shape.createVertex(vertexBuffer, pos, right.x, right.y, 0f, rightNormal.x, rightNormal.y, 0f, uv);
      uv.set(texOffset,depth).mul(trans);
      pos = Shape.createVertex(vertexBuffer, pos, right.x, right.y, modelDef.depth, rightNormal.x, rightNormal.y, 0f, uv);

      int vertOffset =  i * 4;
      edgeBuffer[edgePos++] = (short) (vertOffset);
      edgeBuffer[edgePos++] = (short) (vertOffset + 1);
      edgeBuffer[edgePos++] = (short) (vertOffset + 2);
      edgeBuffer[edgePos++] = (short) (vertOffset + 1);
      edgeBuffer[edgePos++] = (short) (vertOffset + 3);
      edgeBuffer[edgePos++] = (short) (vertOffset + 2);
    }
    MeshPartBuilder meshPartBuilder = modelBuilder.part(this.id, GL20.GL_TRIANGLES, VertexAttributes.Usage.Position | VertexAttributes.Usage.Normal | VertexAttributes.Usage.TextureCoordinates, parent.getStyle().getMaterial());
    meshPartBuilder.addMesh(vertexBuffer, edgeBuffer);
  }


  public void buildFace(ModelBuilder modelBuilder, ModelDef modelDef) {
    ShortArray indices = getTriangles();
    float[] vertexBuffer = new float[this.points.size * 8];
    int pos = 0;

    Matrix3 uvTransform = parent.getStyle().getTransform();
    Material mat = parent.getStyle().getMaterial();
    for (int i = 0; i < this.points.size; i++){
      Vector2 uv = uvs.get(i).mul(uvTransform);
      pos = Shape.createVertex(vertexBuffer, pos, points.get(i), Vector3.Z, uv);
    }
    MeshPartBuilder meshPartBuilder = modelBuilder.part(this.id, GL20.GL_TRIANGLES, VertexAttributes.Usage.Position | VertexAttributes.Usage.Normal | VertexAttributes.Usage.TextureCoordinates, mat);
    meshPartBuilder.addMesh(vertexBuffer, indices.toArray());
  }
}
