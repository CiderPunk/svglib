package net.ciderpunk.svglib.utils;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.utils.Array;

/**
 * Created by matthewlander on 30/12/2015.
 */
public class Bounds {
	public Vector3 min;
	public Vector3 max;
	Vector3 cnt;
	boolean empty;

	public Bounds(Bounds bounds){
		this();
		ext(min);
		ext(max);
	}

	public Bounds(){
		empty= true;
		min = new Vector3();
		max = new Vector3();
		cnt = new Vector3();
	}

	public void reset(){
		min.setZero();
		max.setZero();
		empty = true;
	}

	public void ext(Vector2 val){
		ext(val.x, val.y, 0);
	}

	public void ext(Vector2 val, Vector2 offset){
		ext(val.x + offset.x, val.y + offset.y, 0);
	}

  public void ext(Vector3 val){
		ext(val.x, val.y, val.z);
	}

  /**
   * extend bounds by float vertex array in 3 dimensions
   * @param verts
   */
  public void ext3(float[] verts){
    int i = 0;
    while(i < verts.length){
      this.ext(verts[i++], verts[i++], verts[i++]);
    }
  }


  /**
   * extend bounds by array of vector2
   * @param verts
   */
  public void ext(Vector2[] verts){
    int i = 0;
    for (Vector2 vert : verts) {
      this.ext(vert);
    }
  }




  /**
   * extend bounds by float vertex array in 2 dimensions
   * @param verts
   */
  public void ext2(float[] verts){
    int i = 0;
    while(i < verts.length){
      this.ext(verts[i++], verts[i++], 0);
    }
  }


	public void ext(float x, float y, float z) {
		if (empty){
			min.set(x,y,z);
			max.set(x,y,z);
			empty = false;
		}
		else {
			min.set(min.x > x ? x : min.x, min.y > y ? y : min.y, min.z > z ? z : min.z);
			max.set(max.x < x ? x : max.x, max.y < y ? y : max.y, max.z < z ? z : max.z);
		}
		cnt.setZero();
	}


	public void recenter(){
		getCenter();
		max.sub(cnt);
		min.sub(cnt);
	}


  public Vector3 getCenter(Vector3 val){
    getCenter();
    val.set(cnt);
    return val;
  }

  public Vector2 getCenter(Vector2 val){
    getCenter();
    val.set(cnt.x, cnt.y);
    return val;
  }

  public Vector3 getCenter(){
    return empty ? Vector3.Zero :
            cnt.set(min).add(max).scl(0.5f);
  }

	public float getWidth() {
		return max.x - min.x;
	}

	public float getHeight() {
		return max.y - min.y;
	}
}
