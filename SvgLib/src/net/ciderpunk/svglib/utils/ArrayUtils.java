package net.ciderpunk.svglib.utils;

/**
 * Created by matthewlander on 20/02/2016.
 */
public class ArrayUtils {
	public static <T> T[] reverse(T[] arr){
		for (int i = 0; i < arr.length / 2; i++) {
			T temp = arr[i];
			int opp = arr.length - i - 1;
			arr[i] = arr[opp];
			arr[opp] = temp;
		}
		return arr;
	}


	public static short[] reverse(short[] arr){
		for (int i = 0; i < arr.length / 2; i++) {
			short temp = arr[i];
			int opp = arr.length - i - 1;
			arr[i] = arr[opp];
			arr[opp] = temp;
		}
		return arr;
	}
}
