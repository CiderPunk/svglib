package net.ciderpunk.svglib.utils;

import com.badlogic.gdx.math.Matrix3;
import com.badlogic.gdx.math.Matrix4;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.Array;
import net.ciderpunk.svglib.SvgParseException;


/**
 * Created by Matthew on 23/01/2016.
 */
public class SvgHelper {

  public static Matrix3 parseTransform(String val ) throws SvgParseException {
    return parseTransform(val, true);
  }

  public static Matrix3 parseTransform(String val, boolean correctY ) throws SvgParseException{

    Matrix3 result = new Matrix3();
    if (val == null || val == ""){
      return result;
    }
    else if(val.startsWith("scale(")){
      val = val.substring(6, val.length() - 1);
      String[] parts = val.split(",");
      if (parts.length != 2) {
        throw new SvgParseException("Expected 2 components in scale: \"" + val + "\"");
      }
      result.scale(Float.parseFloat(parts[0]),(correctY ? -1 : 1 ) * Float.parseFloat(parts[1]));

    }
    else if (val.startsWith("matrix(")) {
      val = val.substring(7, val.length() - 1);
      String[] parts = val.split(",");
      if (parts.length != 6) {
        throw new SvgParseException("Expected 6 components in matrix: \"" + val + "\"");
      }
      //inverts matrix across the x axis
      /* no correction */

      if (correctY) {
        result.set(new float[]{
                Float.parseFloat(parts[0]),
                -Float.parseFloat(parts[1]),
                0f,
                Float.parseFloat(parts[2]),
                -Float.parseFloat(parts[3]),
                0f,
                Float.parseFloat(parts[4]),
                Float.parseFloat(parts[5]),
                1f
        });
      }
      else{
        result.set(new float[]{
                Float.parseFloat(parts[0]),
                Float.parseFloat(parts[1]),
                0f,
                Float.parseFloat(parts[2]),
                Float.parseFloat(parts[3]),
                0f,
                Float.parseFloat(parts[4]),
                Float.parseFloat(parts[5]),
                1f
        });
      }


    }
    else if (val.startsWith("translate(")){
      val = val.substring(10, val.length() - 1);
      String[] parts = val.split(",");
      if (parts.length != 2) {
        throw new SvgParseException("Expected 2 components in translate: \"" + val + "\"");
      }
      result.translate(Float.parseFloat(parts[0]), (correctY ? -1 : 1) * Float.parseFloat(parts[1]));

    }
    else {
      throw new SvgParseException("Unknown transform: \"" + val + "\"");
    }
    return result;
  }




  public static boolean getDirection(Array<Vector2> points){
    int tot = 0;
    for (int i = 0; i < points.size; i++) {
      Vector2 here = points.get(i);
      Vector2 next = points.get((i + 1) % points.size);
      tot+=(( next.x - here.x) * (next.y + here.y));
    }
    return (tot < 0);
  }

  /**
   * ensures the polygon defined by the list of points is clockwise oriented
   * @param points
   */
  public static void makeClockwise(Array<Vector2> points) {
    if (getDirection(points)) {
      //reverse those puppies!
      points.reverse();
    }
  }


  public static final Matrix3 invertMatrix = new Matrix3().setToScaling(1,-1);

}
