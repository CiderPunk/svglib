package net.ciderpunk.svglib;

import com.badlogic.gdx.assets.AssetDescriptor;
import com.badlogic.gdx.assets.AssetLoaderParameters;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.assets.loaders.AsynchronousAssetLoader;
import com.badlogic.gdx.assets.loaders.FileHandleResolver;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.utils.Array;
import net.ciderpunk.svglib.pattern.TextureType;

/**
 * Created by Matthew on 03/02/2016.
 */
public class SvgDocLoader extends AsynchronousAssetLoader<SvgDoc, SvgDocLoader.SvgDocParameter> {


  protected final static SvgDocConfig defaultConfig = new SvgDocConfig(true, true, TextureType.DiffuseMap.mask);

  public SvgDocLoader(FileHandleResolver resolver) {
    super(resolver);
  }

  SvgDoc data;

  @Override
  public void loadAsync(AssetManager manager, String fileName, FileHandle file, SvgDocParameter parameter) {

  }

  @Override
  public SvgDoc loadSync(AssetManager manager, String fileName, FileHandle file, SvgDocParameter parameter) {
    SvgDoc doc = data;
    data = null;

    doc.loadDependencies(manager);
    return doc;
  }


  private SvgDoc loadSvg(FileHandle file, SvgDocParameter param){
    SvgDoc doc;
    if (param != null){
      boolean lastState = param.config.assetManaged;
      param.config.assetManaged = true;
      doc = new SvgDoc(file, param.config);
      param.config.assetManaged = lastState;
    }
    else{
      doc = new SvgDoc(file, defaultConfig);
    }
    return doc;
  }


  @Override
  public Array<AssetDescriptor> getDependencies(String fileName, FileHandle file, SvgDocParameter parameter) {
    data = loadSvg(file, parameter);
    return data.getDependencies();
  }

  static public class SvgDocParameter extends AssetLoaderParameters<SvgDoc>{

    public SvgDocConfig config;

    public SvgDocConfig getConfig(){
      return config != null ? config : defaultConfig;
    }

  }
}
