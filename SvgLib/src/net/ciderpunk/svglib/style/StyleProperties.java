package net.ciderpunk.svglib.style;

/**
 * Created by Matthew on 23/01/2016.
 */
public enum StyleProperties {
  Fill("fill"),
  FillOpacity("fill-opacity"),
  //FillRule("fill-rule"),
  Stroke("stroke"),
  StrokeWidth("stroke-width"),
  StrokeOpacity("stroke-opactity"),
  Unknown("unknown");
  private String text;

  StyleProperties(String text){ this.text = text; }
  public static StyleProperties fromString(String val){
    if (val != null){
      for (StyleProperties gt : StyleProperties.values()) {
        if (val.equalsIgnoreCase(gt.text)) {
          return gt;
        }
      }
    }
    return Unknown;
  }
}



