package net.ciderpunk.svglib.style;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g3d.Material;
import com.badlogic.gdx.graphics.g3d.attributes.ColorAttribute;
import com.badlogic.gdx.math.Matrix3;
import com.badlogic.gdx.math.Vector2;
import net.ciderpunk.svglib.SvgDoc;
import net.ciderpunk.svglib.geometry.Geometry;
import net.ciderpunk.svglib.geometry.shapes.Shape;
import net.ciderpunk.svglib.pattern.Pattern;
import net.ciderpunk.svglib.utils.SvgHelper;

/**
 * Created by Matthew on 23/01/2016.
 */
public class Style {
  final Geometry owner;
  public final Color strokeColor = new Color(Color.BLACK);
  public final Color fillColor = new Color(Color.CLEAR);
  private String patternName = null;

  final Matrix3 transform = new Matrix3();
  final Matrix3 edgeTransform = new Matrix3();
  Material material;

  public Style(Geometry owner, String styleDef){
    this.owner = owner;
    //this.shape = shape;
    if (styleDef== null){
      return;
    }
    String[] props = styleDef.split(";");
    for (String prop : props) {
      String[] parts = prop.split(":", 2);
      String value = parts[1].trim();
      float opacity;
      switch(StyleProperties.fromString(parts[0])){
        case Stroke:
          if (value.startsWith("#")){
            opacity = strokeColor.a;
            strokeColor.set(Color.valueOf(value));
            strokeColor.a = opacity;
          }
          break;
        /*
        case StrokeWidth:
          strokeWidth = Float.parseFloat(value);
          break;
        */
        case StrokeOpacity:
          strokeColor.a = Float.parseFloat(value);
          break;
        case Fill:
          if (value.startsWith("url(#")){
            patternName = value.substring(5, value.length() -1);
          }
          else if (value.startsWith("#")){
            opacity = fillColor.a;
            fillColor.set(Color.valueOf(value));
            fillColor.a = opacity;
          }
          break;
        case FillOpacity:
          fillColor.a = Float.parseFloat(value);
          break;
      }
    }
  }


  protected SvgDoc getDoc(){
    return owner.getDoc();
  }


  /**
   * texture transform for front faceing polys
   * @return
   */
  public Matrix3 getTransform(){
    if (material == null){
      getMaterial();
    }
    return transform;
  }

  /**
   * texture transform for edges
   * @return
   */
  public Matrix3 getEdgeTransfom(){
    if (material == null){
      getMaterial();
    }
    return edgeTransform;
  }


static final Vector2 scale = new Vector2();


  public Material getMaterial() {
    if (material == null){
      if (patternName != null) {
        SvgDoc doc = getDoc();
        Pattern pattern = doc.getPattern(patternName);
        material = pattern.getMaterial(this.transform);
      }
      //this.transform.mulLeft(SvgHelper.invertMatrix);
      //failed finding pattern or none specified
      if (material == null){
        material = new Material(ColorAttribute.createDiffuse(this.fillColor));
        //no point doing fancy transforms for flat colour!
        this.transform.setToScaling(1,-1);
      }
      transform.getScale(scale);
      edgeTransform.setToScaling(scale);


    }
    return material;
  }
}
