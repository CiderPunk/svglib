package net.ciderpunk.svglib;

import net.ciderpunk.svglib.pattern.TextureType;

/**
 * Created by Matthew on 03/02/2016.
 */
public class SvgDocConfig{

  public int textureMask = TextureType.DiffuseMap.mask;
  public boolean loadTextures = true;
  public boolean assetManaged = false;

  public SvgDocConfig() { }

  public SvgDocConfig(boolean loadTextures) {
    this(loadTextures, false,  TextureType.DiffuseMap.mask);
  }

  public SvgDocConfig(boolean loadTextures, boolean assetManaged, int textureMask) {
    this.loadTextures = loadTextures;
    this.assetManaged = assetManaged;
    this.textureMask = textureMask;
  }

  public SvgDocConfig(SvgDocConfig source){
    this(source.loadTextures, source.assetManaged, source.textureMask);
  }

  public SvgDocConfig copy(){
    return new SvgDocConfig(this);
  }

}
