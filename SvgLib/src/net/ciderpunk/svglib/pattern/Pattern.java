package net.ciderpunk.svglib.pattern;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g3d.Material;
import com.badlogic.gdx.math.Matrix3;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.XmlReader;
import net.ciderpunk.svglib.SvgDoc;
import net.ciderpunk.svglib.SvgParseException;
import net.ciderpunk.svglib.utils.SvgHelper;

/**
 * Created by Matthew on 02/02/2016.
 */
public class Pattern {

  /**
   * pattern id
   */
  public final String id;
  /**
   * linkId chained patter
   */
  public final String linkId;
  /**
   * doc svg doc
   */
  public final SvgDoc doc;
  /**
   * pattern transformation
   */
  public final Matrix3 transform;

 // static final Vector2 temp = new Vector2();

  public final Image image;


  public Pattern(XmlReader.Element element, SvgDoc svgDoc){
    this.id = element.getAttribute("id");
    this.doc = svgDoc;
    String transAttr = element.getAttribute("patternTransform", null);
    this.transform = new Matrix3();

    if (transAttr != null) {
      try {
        this.transform.set(SvgHelper.parseTransform(transAttr, false));
        this.transform.inv();
      } catch (SvgParseException e) {
        e.printStackTrace();
      }
    }

/* only supporting image width and height
    width = Float.parseFloat(element.getAttribute("width", "0"));
    height = Float.parseFloat(element.getAttribute("height", "0"));
*/
    String link = element.getAttribute("xlink:href", null);
    //remove '#' prefix
    linkId = (link!= null ? link.substring(1) : null);

    XmlReader.Element imageEl = element.getChildByName("image");
    image = imageEl != null ? new Image(imageEl) : null;
  }

  static final Matrix3 temp = new Matrix3();

  public Material getMaterial(Matrix3 transform) {
    transform.mulLeft(this.transform);
    Material material = null;
    if (linkId != null){
      material = doc.getPattern(linkId).getMaterial(transform);
    }
    else if (image != null){
      //apply sacling of the texture
      transform.mulLeft(temp.setToScaling(image.size).inv());
      material = image.getMaterial();
    }
    return material;
  }
}
