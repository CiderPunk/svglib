package net.ciderpunk.svglib.pattern;

import com.badlogic.gdx.assets.AssetDescriptor;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g3d.Material;
import com.badlogic.gdx.graphics.g3d.attributes.TextureAttribute;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.Disposable;
import com.badlogic.gdx.utils.XmlReader;
import net.ciderpunk.svglib.SvgDocConfig;

/**
 * Created by Matthew on 02/02/2016.
 */
public class Image implements Disposable{

  public final String path;
  public final String id;

  public final Vector2 size;

  //FIXME: replace these with an array
  Texture diffuse;
  Texture normal;

  public Image(XmlReader.Element element){
    id = element.getAttribute("id", null);
    path = element.getAttribute("xlink:href", null).replace('\\', '/');
    size = new Vector2(Float.parseFloat(element.getAttribute("width", "0")), Float.parseFloat(element.getAttribute("height", "0")));

  }

  /**
   *
   * @param basedir
   * @param textureMask
   * @param assetMan
   */
  public void loadDependencies(FileHandle basedir, int textureMask, AssetManager assetMan) {
    FileHandle file = basedir.child(path) ;
    if (TextureType.hasType(textureMask, TextureType.DiffuseMap)){
      diffuse = assetMan.get(file.path(), Texture.class);
      diffuse.setWrap(Texture.TextureWrap.Repeat, Texture.TextureWrap.Repeat);
    }
    if (TextureType.hasType(textureMask, TextureType.NormalMap)){
      normal = assetMan.get(TextureType.NormalMap.generateFileHandle(file).path(), Texture.class);
      normal.setWrap(Texture.TextureWrap.Repeat, Texture.TextureWrap.Repeat);
    }
  }

  /**
   *
   * @param basedir
   * @param textureMask
   */
  public void loadTextues(FileHandle basedir, int textureMask) {
    FileHandle file = basedir.child(path) ;
    if (TextureType.hasType(textureMask, TextureType.DiffuseMap)){
      diffuse = new Texture(file);
      diffuse.setWrap(Texture.TextureWrap.Repeat, Texture.TextureWrap.Repeat);
    }
    if (TextureType.hasType(textureMask, TextureType.NormalMap)){
      normal = new Texture(TextureType.NormalMap.generateFileHandle(file));
      normal.setWrap(Texture.TextureWrap.Repeat, Texture.TextureWrap.Repeat);
    }
  }

  /**
   *
   * @param basedir
   * @param textureMask
   * @param dependencies
   */
  public void getDependencies(FileHandle basedir, int textureMask, Array<AssetDescriptor> dependencies){
    FileHandle file = basedir.child(path);
    if (TextureType.hasType(textureMask, TextureType.DiffuseMap)){
      dependencies.add(new AssetDescriptor(TextureType.DiffuseMap.generateFileHandle(file), Texture.class));
    }
    if (TextureType.hasType(textureMask, TextureType.NormalMap)){
      dependencies.add(new AssetDescriptor(TextureType.NormalMap.generateFileHandle(file), Texture.class));
    }
  }

  @Override
  public void dispose() {
    if (diffuse != null){
      diffuse.dispose();
    }
    if (normal != null){
      normal.dispose();
    }
  }

  public Material getMaterial() {
    Material material = new Material();
    if (diffuse != null) {
      material.set(TextureAttribute.createDiffuse(diffuse));
      material.set();
    }
    if (normal != null) {
      material.set(TextureAttribute.createNormal(normal));
    }
    return material;
  }
}
