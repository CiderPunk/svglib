package net.ciderpunk.svglib.pattern;

import com.badlogic.gdx.files.FileHandle;

/**
 * Created by Matthew on 04/02/2016.
 */
public enum TextureType {

  DiffuseMap(1, ""),
  NormalMap(2, "_nml");


  public final int mask;
  private String fileNamePart;

  TextureType(int mask, String fileNamePart){
    this.mask = mask;
    this.fileNamePart = fileNamePart;
  }


  public FileHandle generateFileHandle( FileHandle file){
    return file.parent().child(file.nameWithoutExtension() + this.fileNamePart + file.extension());
  }

  public static boolean hasType(int mask, TextureType t){
    return ((mask | t.mask) == mask);
  }

  public static int buildMask(TextureType... types){
    int mask = 0;
    for (TextureType type : types) {
      mask |= type.mask;
    }
    return mask;
  }


}
