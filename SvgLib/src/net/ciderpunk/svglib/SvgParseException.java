package net.ciderpunk.svglib;

/**
 * Created by Matthew on 23/01/2016.
 */
public class SvgParseException extends Exception {
  public SvgParseException() {
    super();
  }
  public SvgParseException(String message) {
    super(message);
  }
  public SvgParseException(Throwable cause) {
    super(cause);
  }
  public SvgParseException(String message, Throwable cause) {
    super(message, cause);
  }
}
