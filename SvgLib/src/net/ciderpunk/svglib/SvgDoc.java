package net.ciderpunk.svglib;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.assets.AssetDescriptor;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.g3d.Environment;
import com.badlogic.gdx.graphics.g3d.Model;
import com.badlogic.gdx.graphics.g3d.ModelBatch;
import com.badlogic.gdx.graphics.g3d.ModelInstance;
import com.badlogic.gdx.graphics.g3d.utils.ModelBuilder;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.Matrix4;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.Disposable;
import com.badlogic.gdx.utils.ObjectMap;
import com.badlogic.gdx.utils.XmlReader;
import com.badlogic.gdx.utils.XmlReader.*;
import net.ciderpunk.svglib.geometry.Geometry;
import net.ciderpunk.svglib.geometry.Group;
import net.ciderpunk.svglib.geometry.ModelDef;
import net.ciderpunk.svglib.geometry.SvgRoot;
import net.ciderpunk.svglib.pattern.Pattern;

import java.io.IOException;

/**
 * Created by Matthew on 22/01/2016.
 */
public class SvgDoc implements Disposable{


  public static final SvgDocConfig defaultConfig = new SvgDocConfig();

  final SvgDocConfig config ;
  //public final Array<Group> groups;
  public final ObjectMap<String, Pattern> patterns;
  final FileHandle file;
  private static final Matrix4 identityM4 = new Matrix4().idt();


  public final SvgRoot root;


  public SvgDoc(String path, SvgDocConfig config){
    this(Gdx.files.internal(path), config);
  }
  public SvgDoc(String path){
    this(path, defaultConfig );
  }
  public SvgDoc(FileHandle fileHandle){ this(fileHandle, defaultConfig );  }

  public SvgDoc(FileHandle fileHandle, SvgDocConfig config){
    this.config = new SvgDocConfig(config);
    this.file = fileHandle;
    XmlReader reader = new XmlReader();
    Element rootEl = null;
    try{
     rootEl = reader.parse(fileHandle);
    }catch (IOException e) {
      e.printStackTrace();
    }

    //get defs / patterns etc
    //i sure hope there's only ever one of these!
    Element defs = rootEl.getChildByName("defs");
    patterns = new ObjectMap<String, Pattern>();
    if (defs != null) {
      Array<XmlReader.Element> patternEls = defs.getChildrenByName("pattern");
      //build map of patterns
      for (Element patternEl : patternEls) {
        Pattern pattern = new Pattern(patternEl, this);
        patterns.put(pattern.id, pattern);
      }
    }


    this.root = new SvgRoot(this, null, rootEl);
    /*
    groups = new Array<Group>();
    //get groups
    Array<XmlReader.Element> groupEls = rootEl.getChildrenByName("g");
    int count = 0;
    for (Element groupEl : groupEls) {
      Group group = new Group(this,null, groupEl);
      groups.add(group);
    }
    */
    if (!config.assetManaged && config.loadTextures){
      //preload textureMask!
      preloadTextures();
    }

  }
/*
  public void draw(ShapeRenderer shaper) {
    for (Group group : groups) {
      group.draw(shaper);
    }
    shaper.setTransformMatrix(identityM4);
  }
*/

  /*
  public Group getGroup(String name){
    for (Group group : groups) {
      if (name.equals(group.id)){
        return group;
      }
    }
    return null;
  }

  public Group getGroup(int id){
    return groups.get(id);
  }
*/

  @Override
  public void dispose() {
    root.dispose();
    //only need to clear these if the
    if (!config.assetManaged){
      for (Pattern pattern : patterns.values()) {
        if (pattern.image != null){
          pattern.image.dispose();
        }
      }

    }
  }

  public Pattern getPattern(String name){
    return patterns.get(name, null);
  }

  public void preloadTextures(){
    for (Pattern pattern : patterns.values()) {
      if (pattern.image != null){
        pattern.image.loadTextues(file.parent(), config.textureMask);
      }
    }
  }


  public Array<AssetDescriptor> getDependencies(){
    Array<AssetDescriptor> deps  = new Array();
    if (config.loadTextures){
      FileHandle baseDir = this.file.parent();
      for (Pattern pattern : patterns.values()) {
        if (pattern.image != null){
          pattern.image.getDependencies(baseDir, config.textureMask, deps);
        }
      }
    }
    return deps;
  }

  public void loadDependencies(AssetManager manager) {
    if (config.loadTextures){
      FileHandle baseDir = this.file.parent();
      for (Pattern pattern : patterns.values()) {
        if (pattern.image != null){
          pattern.image.loadDependencies(baseDir, config.textureMask, manager);
        }
      }
    }
  }


}
