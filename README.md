# README #

SvgLib is a library to take SVG files and use them as level data for 2D / partially 3D games, it relies heavily on LibGDX, and Box2D. 

### What is this repository for? ###

SvgLib is designed to create Box2D bodies and fixtures from paths and simple shapes in SVG documents.
It also creates 2d and 3d models by extruding from these shapes for speedy drawing in libGdx games.
It applies patterns specified in the SVG to the shapes accurately recreating textures specified in the SVG, patterns must be imported png textures included in the path of the SVG, I'll write instructions on how to do this later!

### How do I get set up? ###

There's the library itself svglib and the test project svgtest which is just a libgdx desktop project. to run, ensure the desktop build is running in core/assets
it should display an SVG from core/assets/ in 2d model mode, switch between modes:
F1 - Shape renderer, this should fill (for non-paths) and use the stroke colour specified in the SVG it's of limited use just for debugging parsing.
F2 - Box2D debug renderer, shows how shapes are decomposed into the fixtures
F3 - shows the 3d model rendered in an orthographic camera, sames position and orientation as the box2d/shaperenderer view
F4 - 3d view of model.

What's supported:
Rectangles - shape renderer,  box2d, models
circles - shape renderer, box2d, models
ellipse - just shape renderer for now
paths - straight-edged loops are recreated in shape renderer (not filled), box2d, and models
it won't bother trying to render splines and just view them as straight lines so best avoid them for now!

### Contribution guidelines ###

I welcome any feedback or suggestions on code style or anything!
pull requests are of course welcome!

### Who do I talk to? ###

Contact [@Ciderpunk](https://twitter.com/CiderPunk) on twitter