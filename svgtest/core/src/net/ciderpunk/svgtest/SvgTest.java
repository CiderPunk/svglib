package net.ciderpunk.svgtest;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.graphics.*;
import com.badlogic.gdx.graphics.g3d.Environment;
import com.badlogic.gdx.graphics.g3d.ModelBatch;
import com.badlogic.gdx.graphics.g3d.attributes.ColorAttribute;
import com.badlogic.gdx.graphics.g3d.environment.DirectionalLight;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.Box2DDebugRenderer;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.utils.viewport.FitViewport;
import com.badlogic.gdx.utils.viewport.Viewport;
import net.ciderpunk.svglib.SvgDoc;
import net.ciderpunk.svglib.geometry.Group;
import net.ciderpunk.svglib.geometry.ModelDef;

public class SvgTest extends ApplicationAdapter implements InputProcessor {
  Viewport view;

  RenderMode mode = RenderMode.Model2d;
  Box2DDebugRenderer debugRenderer;
  ModelBatch modelBatch;
  Environment env;
  SvgDoc doc;



  AssetManager assMan;

	PerspectiveCamera perspectiveCamera;

  World world;

  enum RenderMode{
    Box2d,
    Model2d,
    Model3d,
  }




	Group dynamicGroup;


	@Override
	public void create () {

    assMan = new AssetManager();

    //view
    Camera cam = new OrthographicCamera();
    view = new FitViewport(1024,768, cam);
    cam.update();
    //input
    Gdx.input.setInputProcessor(this);

		//load svg file...
    doc = new SvgDoc("simpletex.svg");
    //doc = new SvgDoc("fall.svg");

    //box 2d init..
    debugRenderer = new Box2DDebugRenderer();
    world = new World(new Vector2(0,-10f), true);

    for (Group group : doc.root.getGroups()) {
      if (group.id.equals("dynamic")){
        FixtureDef fixtureDef = new FixtureDef();
        fixtureDef.density = 1f;
        BodyDef bodyDef = new BodyDef();
        bodyDef.type = BodyDef.BodyType.DynamicBody;
        group.addToWorld(world, bodyDef, fixtureDef);
        //init models
        ModelDef modelDef = new ModelDef();
        modelDef.depth = -64f;
        modelDef.smoothThreshold = 20f;
        modelDef.verticiesPerRadiusUnit = 0.5f;
				modelDef.zIndex = 0;
        group.buildModel(modelDef);
        dynamicGroup = group;
      }
      else{
        FixtureDef fixtureDef = new FixtureDef();
        fixtureDef.density = 1f;
        BodyDef bodyDef = new BodyDef();
        bodyDef.type = BodyDef.BodyType.StaticBody;
        group.addToWorld(world, bodyDef, fixtureDef);
        //init models
        ModelDef modelDef = new ModelDef();
        modelDef.depth = -128f;
        modelDef.smoothThreshold = 20f;
        modelDef.verticiesPerRadiusUnit = 0.5f;
				modelDef.zIndex = 0;
        group.buildModel(modelDef);

      }
    }


    //3d view stuff
		perspectiveCamera = new PerspectiveCamera(67, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
		perspectiveCamera.position.set(250f,250f,250f);
		perspectiveCamera.lookAt(0,0,0);
		perspectiveCamera.near = 1f;
		perspectiveCamera.far = 5000f;
		perspectiveCamera.update();

    modelBatch = new ModelBatch();
    env = new Environment();
    env.set(new ColorAttribute(ColorAttribute.AmbientLight,0.4f, 0.3f, 0.3f, 1.0f));

    Vector3 lightDir = new Vector3(-1f,0f,-0.5f).nor();
    env.add(new DirectionalLight().set(Color.WHITE, lightDir));
  }


  @Override
  public void resize(int width, int height) {
    super.resize(width, height);
    view.update(width, height);
  }


  @Override
	public void render () {
    if (assMan.update()) {
      switch (mode) {
        case Box2d:
          renderBox();
          break;
        case Model2d:
          renderModel2d();
          break;
        case Model3d:
          renderModel3d();
          break;
      }
      update();
    }
	}

  final float SimulationSpeed = 1f/60f;
  float time = 0f;
  float accumulator = 0f;

  private void update() {
    float dT = Gdx.graphics.getDeltaTime();
    if (dT > 0.5f){
      //no point catching up over half a second, skip this one
      return;
    }
    time += dT;
    accumulator +=dT;
    while(accumulator >= SimulationSpeed ) {
      accumulator -= SimulationSpeed;
      world.step(SimulationSpeed, 6, 2);
    }

    if (dynamicGroup!= null){
      dynamicGroup.update();
    }

  }

  private void renderBox() {
    Gdx.gl.glClearColor(0, 0, 0, 1);
    Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
    debugRenderer.render(world, view.getCamera().projection);
  }

  private void renderModel2d() {
    Gdx.gl.glClearColor(0, 0, 0, 1);
    Gdx.gl.glViewport(0, 0, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
    Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
    //Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT | GL20.GL_DEPTH_BUFFER_BIT);
    modelBatch.begin(view.getCamera());

    for (Group group : doc.root.getGroups()) {
      group.draw(modelBatch, env);
    }
		//fallGroup.draw(modelBatch, env);
    modelBatch.end();

  }



	private void renderModel3d() {
		Gdx.gl.glClearColor(0, 0, 0, 1);
		Gdx.gl.glViewport(0, 0, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT | GL20.GL_DEPTH_BUFFER_BIT);
		modelBatch.begin(perspectiveCamera);
    for (Group group : doc.root.getGroups()) {
      group.draw(modelBatch, env);
    }
		//fallGroup.draw(modelBatch, env);
		modelBatch.end();

	}


  @Override
  public void dispose() {

    doc.dispose();
    modelBatch.dispose();
    debugRenderer.dispose();
    world.dispose();
  }

  @Override
  public boolean keyDown(int keycode) {
    switch (keycode){
      case 244:
        mode = RenderMode.Box2d;
        break;
      case 245:
        mode= RenderMode.Model2d;
        break;
      case 246:
        mode= RenderMode.Model3d;
        break;

    }
    //Gdx.app.log("key","mask: " +  keycode);
    return true;
  }

  @Override
  public boolean keyUp(int keycode) {
    return false;
  }

  @Override
  public boolean keyTyped(char character) {
    return false;
  }

  @Override
  public boolean touchDown(int screenX, int screenY, int pointer, int button) {
    return false;
  }

  @Override
  public boolean touchUp(int screenX, int screenY, int pointer, int button) {
    return false;
  }

  @Override
  public boolean touchDragged(int screenX, int screenY, int pointer) {
    return false;
  }

  @Override
  public boolean mouseMoved(int screenX, int screenY) {
    return false;
  }

  @Override
  public boolean scrolled(int amount) {
    return false;
  }
}

